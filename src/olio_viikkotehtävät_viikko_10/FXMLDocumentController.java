/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_viikkotehtävät_viikko_10;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;

/**
 *
 * @author Jönnsson
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Button previousButton;
    @FXML
    private Button nextButton;
    @FXML
    private Button refreshButton;
    @FXML
    private TextField urlBox;
    @FXML
    private WebView web;
    @FXML
    private Button sendCommand;

    String magic;
    
    ArrayList<Object> al = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        web.getEngine().load("http://www.google.fi");
        web.getEngine().getHistory().setMaxSize(20);
        // TODO
    }    

    @FXML
    private void goPreviousPage(ActionEvent event) {      
        web.getEngine().getHistory().go(-1);
    }

    @FXML
    private void goNextPage(ActionEvent event) {
        web.getEngine().getHistory().go(1);
    }

    @FXML
    private void refresh(ActionEvent event) {
        web.getEngine().reload();
    }

    @FXML
    private void loadWebpage(ActionEvent event) {
        if("index.html".equals(urlBox.getText())){
            web.getEngine().load(getClass().getResource("index2.html").toExternalForm());
        }
        else{
            web.getEngine().load("http://"+ urlBox.getText());
        }
    }

    @FXML
    private void doMagic(ActionEvent event) {
        if("done".equals(magic)){
            web.getEngine().executeScript("initialize()");
            magic = "neverhappens";
        }
        else{
        web.getEngine().executeScript("document.shoutOut()");
        magic = "done";
        }
    }

    @FXML
    private void getUrl(MouseEvent event) {
    }
    
}
